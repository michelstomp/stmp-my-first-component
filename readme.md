# Component Boilerplate

## Instructies nieuw component
Volg de instructies bij een nieuw component.

1. Pas bower.json aan.
2. Zorg dat alle sass bestanden in de all.scss wordt aangeroepen.
3. Houd rekening met class prefixes. Gebruik altijd de componentnaam als prefix.

## Toevoegen van component aan project
1. Include de sass toe met `bower_components/componentnaam/sass/all.scss`
2. Include de js toe aan de require dependencies `bower_components/componentnaam/js/module.js`
3. Kijk naar de index.html voor html voorbeelden

## Features
- List features here.

## Settings
(example below)

Parameter  | Type | Default | Description
---------- | ---- | ------- | -----------
fade       | bool | false   | Should component fade-in

